#!/usr/bin/env bash

readonly TV_REPO='https://wqureshi@bitbucket.org/wqureshi/tv-stuff.git'

# install brew
# Check that the advocate things home dir is set
echo -n 'Check Dependency -> Homebrew: '
if [[ $(command -v brew) > /dev/null ]]; then
    echo 'OK'
else
    echo 'Not Found. Installing :)'
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# install git
# check if git is installed
echo -n 'Check Dependency -> Git: '
git --version &> /dev/null
if [[ $? != 0 ]]; then
    echo 'Not Found. Installing :)'
    brew install git
else
    echo 'OK'
fi

# clone tv repo
echo -n 'Checking tv stuff status: '
if [[ -d ~/tv-stuff ]]; then
    echo 'Updating...'
    cd ~/tv-stuff
    git pull origin master
else
    echo 'Installing...'
    cd ~
    git clone "$TV_REPO"
fi

echo 'Checking aliases...'
# tell script to expand aliases when it finds them and then source the alias file
shopt -s expand_aliases
source ~/.bash_profile
unalias tv &> /dev/null
unalias tv-update &> /dev/null
unalias tv-dir &> /dev/null
source ~/.bash_profile

output=$(alias | grep tv=)
if [[ $output == '' ]]; then
    echo 'Adding alias: tv'
    echo 'alias tv="open ~/tv-stuff/index.html"' >> ~/.bash_profile
fi

output=$(alias | grep tv-update=)
if [[ $output == '' ]]; then
    echo 'Adding alias: tv-update'
    echo 'alias tv-update="cd ~/tv-stuff; git pull origin master;"' >> ~/.bash_profile
fi

output=$(alias | grep tv-dir=)
if [[ $output == '' ]]; then
    echo 'Adding alias: tv-dir'
    echo 'alias tv-dir="cd ~/tv-stuff"' >> ~/.bash_profile
fi

source ~/.bash_profile

echo 'Checking Symblink...'
if [[ ! -f ~/Desktop/tv.html ]]; then
    echo 'Creating Symblink on desktop...'
    ln -s ~/tv-stuff/index.html ~/Desktop/tv.html
fi

echo 'Done, please re-open shell to continue...'

# open html page
open ~/tv-stuff/index.html
